// Package datastore contains a fake (and partial) implementation of App Engine's datastore.
package datastore

// TODO(seckar): make all calls threadsafe
// TODO(seckar): support transactions

import (
	"encoding/binary"
	"fmt"
	"reflect"
	"sync"

	"code.google.com/p/goprotobuf/proto"

	ds "appengine_internal/datastore"

	aeds "appengine/datastore"
	aei "appengine_internal"
)

type entityKey string

type entity struct {
	version int64
	pb      *ds.EntityProto
}

// Instance stores all state for a single datastore instance.
type Instance struct {
	contents          map[entityKey]*entity
	nextID            int64
	nextTransactionId uint64
	transactions      map[uint64]*transactionState
	mutex             sync.Mutex
}

type scopeEntry struct {
	version int64
	pb      *ds.EntityProto
	dirty   bool
}

type transactionState struct {
	instance *Instance
	scope    map[entityKey]*scopeEntry
}

// NewInstance returns a new, clean datastore instance.
func NewInstance() *Instance {
	return &Instance{
		contents:     make(map[entityKey]*entity),
		transactions: make(map[uint64]*transactionState),
	}
}

func (i *Instance) transaction(t *ds.Transaction) (*transactionState, error) {
	if st, ok := i.transactions[*t.Handle]; ok {
		return st, nil
	} else {
		return nil, fmt.Errorf("invalid transaction: %v", t)
	}
}

// HandleRequest performs a datastore call
func (i *Instance) HandleRequest(
	service, method string,
	in, out aei.ProtoMessage,
	opts *aei.CallOptions) error {
	if service != "datastore_v3" {
		return fmt.Errorf("unsupported service: %s.%s", service, method)
	}

	switch method {
	case "RunQuery":
		return i.runQuery(in.(*ds.Query), out.(*ds.QueryResult))
	case "Put":
		return i.put(in.(*ds.PutRequest), out.(*ds.PutResponse))
	case "Get":
		return i.get(in.(*ds.GetRequest), out.(*ds.GetResponse))
	case "Delete":
		return i.delete(in.(*ds.DeleteRequest), out.(*ds.DeleteResponse))
	case "BeginTransaction":
		return i.beginTransaction(in.(*ds.BeginTransactionRequest), out.(*ds.Transaction))
	case "Rollback":
		return i.rollback(in.(*ds.Transaction), out.(*ds.VoidProto))
	case "Commit":
		return i.commit(in.(*ds.Transaction), out.(*ds.CommitResponse))
	}

	return fmt.Errorf(
		"Unsupported datastore method: %s taking %s, returns %s",
		method, reflect.TypeOf(in), reflect.TypeOf(out))
}

func appendVarint(b []byte, i int64) []byte {
	var buf [10]byte
	return append(b, buf[0:binary.PutVarint(buf[:], i)]...)
}

func encodeKey(k *ds.Reference) (entityKey, error) {
	// TODO(seckar) Find out what entity groups and namespaces for.
	var r []byte
	for _, e := range k.GetPath().GetElement() {
		r = appendVarint(r, int64(len(*e.Type)))
		r = append(r, []byte(*e.Type)...)

		if e.Id != nil {
			r = appendVarint(append(r, 0), *e.Id)
		} else if e.Name != nil {
			r = appendVarint(append(r, 1), int64(len(*e.Name)))
			r = append(r, (*e.Name)...)
		} else {
			return entityKey(""), aeds.ErrInvalidKey
		}
	}
	return entityKey(r), nil
}

func (i *Instance) completeKey(k *ds.Reference) (*ds.Reference, error) {
	r := new(ds.Path)
	for _, e := range k.GetPath().GetElement() {
		if e.Name == nil && e.Id == nil {
			// Entity has an incomplete key, use the next id
			i.nextID++
			e = &ds.Path_Element{
				Type: e.Type,
				Id:   proto.Int64(i.nextID),
			}
		}
		r.Element = append(r.Element, e)
	}
	return &ds.Reference{
		App:       k.App,
		NameSpace: k.NameSpace,
		Path:      r,
	}, nil
}

func (i *Instance) put(in *ds.PutRequest, out *ds.PutResponse) error {
	for _, e := range in.Entity {
		k, err := i.completeKey(e.Key)
		if err != nil {
			return err
		}

		enc, err := encodeKey(k)
		if err != nil {
			return err
		}

		pb := proto.Clone(e).(*ds.EntityProto)
		if in.Transaction != nil {
			txn, err := i.transaction(in.Transaction)
			if err != nil {
				return err
			}

			if se, ok := txn.scope[enc]; ok {
				se.pb = pb
				se.dirty = true
			} else {
				txn.scope[enc] = &scopeEntry{
					pb:      pb,
					version: 1,
					dirty:   true,
				}
			}
		} else {
			ent, ok := i.contents[enc]
			if !ok {
				ent = new(entity)
				i.contents[enc] = ent
			}
			ent.pb = proto.Clone(e).(*ds.EntityProto)
			ent.version++
		}

		out.Key = append(out.Key, k)
	}
	return nil
}

func (i *Instance) get(in *ds.GetRequest, out *ds.GetResponse) error {
	for _, k := range in.Key {
		enc, err := encodeKey(k)
		if err != nil {
			return err
		}

		e, exists := i.contents[enc]
		if !exists {
			return aeds.ErrNoSuchEntity
		}

		out.Entity = append(out.Entity, &ds.GetResponse_Entity{
			Entity: e.pb,
			Key:    k,
		})
	}
	out.InOrder = proto.Bool(true)

	return nil
}

func (i *Instance) delete(in *ds.DeleteRequest, out *ds.DeleteResponse) error {
	for _, k := range in.Key {
		enc, err := encodeKey(k)
		if err != nil {
			return err
		}

		_, exists := i.contents[enc]
		if !exists {
			return aeds.ErrNoSuchEntity
		}
		delete(i.contents, enc)
	}
	return nil
}

func (i *Instance) beginTransaction(in *ds.BeginTransactionRequest, out *ds.Transaction) error {
	i.mutex.Lock()
	defer i.mutex.Unlock()

	id := i.nextTransactionId
	i.nextTransactionId += 1

	out.Handle = proto.Uint64(id)
	i.transactions[id] = &transactionState{
		instance: i,
		scope:    make(map[entityKey]*scopeEntry),
	}

	return nil
}

func (i *Instance) rollback(in *ds.Transaction, out *ds.VoidProto) error {
	return nil
}

func (i *Instance) commit(in *ds.Transaction, out *ds.CommitResponse) error {
	return nil
}
