package datastore

import (
	"sort"
	"strings"

	"code.google.com/p/goprotobuf/proto"

	ds "appengine_internal/datastore"
)

func descendsFrom(cand, ancestor *ds.Reference) bool {
	encCand, err := encodeKey(cand)
	if err != nil {
		return false
	}

	encAnc, err := encodeKey(ancestor)
	if err != nil {
		return false
	}

	return strings.HasPrefix(string(encCand), string(encAnc))
}

func (i *Instance) runQuery(in *ds.Query, out *ds.QueryResult) error {
	var r []*ds.EntityProto
	for _, e := range i.contents {
		elems := e.pb.GetKey().GetPath().GetElement()
		if in.Kind != nil && elems[len(elems)-1].GetType() != in.GetKind() {
			continue
		}

		if in.Ancestor != nil && !descendsFrom(e.pb.Key, in.Ancestor) {
			continue
		}

		rejected := false
		for _, f := range in.Filter {
			if !filterIncludes(f, e.pb) {
				rejected = true
				break
			}
		}
		if rejected {
			continue
		}

		// Still here? It's a match.
		r = append(r, proto.Clone(e.pb).(*ds.EntityProto))
	}

	// TODO(seckar): Sort by orders
	sort.Sort(&orderer{in.Order, r})

	if in.GetOffset() > 0 {
		out.SkippedResults = proto.Int32(in.GetOffset())
		if len(r) < int(in.GetOffset()) {
			r = r[in.GetOffset():]
		} else {
			r = r[0:0]
		}
	}

	if in.Limit != nil && len(r) > int(*in.Limit) {
		out.MoreResults = proto.Bool(true)
		r = r[0:*in.Limit]
	}

	out.Result = r

	return nil
}

func getPropertyValue(p *ds.PropertyValue) interface{} {
	if p.Int64Value != nil {
		return p.Int64Value
	} else if p.BooleanValue != nil {
		return p.BooleanValue
	} else if p.StringValue != nil {
		return p.StringValue
	} else if p.DoubleValue != nil {
		return p.DoubleValue
	} else if p.Pointvalue != nil {
		return p.Pointvalue
	} else if p.Uservalue != nil {
		return p.Uservalue
	} else if p.Referencevalue != nil {
		return p.Referencevalue
	} else {
		return nil
	}
}

func filterIncludes(f *ds.Query_Filter, e *ds.EntityProto) bool {
	for _, fp := range f.Property {
		for _, ep := range e.Property {
			if fp.GetName() == ep.GetName() {
				if compareValue(f.GetOp(), ep.Value, fp.Value) {
					return true
				}
			}
		}
	}
	return false
}

func compareLessThan(lhs, rhs interface{}) bool {
	switch lhs.(type) {
	case *int64:
		rhv, ok := rhs.(*int64)
		return ok && *lhs.(*int64) < *rhv
	case *bool:
		rhv, ok := rhs.(*bool)
		return ok && (!*lhs.(*bool) || *rhv)
	case *string:
		rhv, ok := rhs.(*string)
		return ok && *lhs.(*string) < *rhv
	case *float64:
		rhv, ok := rhs.(*float64)
		return ok && *lhs.(*float64) < *rhv
	case *ds.PropertyValue_PointValue:
		rhv, ok := rhs.(*ds.PropertyValue_PointValue)
		lhv := lhs.(*ds.PropertyValue_PointValue)
		if !ok {
			return false
		}
		if lhv.GetX() != rhv.GetX() {
			return lhv.GetX() < rhv.GetX()
		}
		return lhv.GetY() < rhv.GetY()
	case *ds.PropertyValue_UserValue:
		rhv, ok := rhs.(*ds.PropertyValue_UserValue)
		lhv := lhs.(*ds.PropertyValue_UserValue)
		// TODO(seckar): Compare other fields too.
		return ok && lhv.GetEmail() < rhv.GetEmail()
	case *ds.PropertyValue_ReferenceValue:
		rhv, ok := rhs.(*ds.PropertyValue_ReferenceValue)
		lhv := lhs.(*ds.PropertyValue_ReferenceValue)
		if !ok {
			return false
		}
		for i, lhe := range lhv.Pathelement {
			if i >= len(rhv.Pathelement) {
				return false
			}
			rhe := rhv.Pathelement[i]

			if lhe.GetType() != rhe.GetType() {
				return lhe.GetType() < rhe.GetType()
			}
			if lhe.GetId() != rhe.GetId() {
				return lhe.GetId() < rhe.GetId()
			}
			return lhe.GetName() < rhe.GetName()
		}
	}
	return false
}

func compareValue(op ds.Query_Filter_Operator, lhp, rhp *ds.PropertyValue) bool {
	lhs, rhs := getPropertyValue(lhp), getPropertyValue(rhp)
	switch op {
	case ds.Query_Filter_LESS_THAN:
		return compareLessThan(lhs, rhs)
	case ds.Query_Filter_LESS_THAN_OR_EQUAL:
		return !compareLessThan(rhs, lhs)
	case ds.Query_Filter_GREATER_THAN:
		return compareLessThan(rhs, lhs)
	case ds.Query_Filter_GREATER_THAN_OR_EQUAL:
		return !compareLessThan(lhs, rhs)
	case ds.Query_Filter_EQUAL:
		return !compareLessThan(lhs, rhs) && !compareLessThan(rhs, lhs)
	case ds.Query_Filter_IN:
		return false
	case ds.Query_Filter_EXISTS:
		return true
	}
	return false
}

type orderer struct {
	Order    []*ds.Query_Order
	Entities []*ds.EntityProto
}

func (o *orderer) Len() int {
	return len(o.Entities)
}

func (o *orderer) Less(i, j int) bool {
	return o.compare(o.Entities[i], o.Entities[j])
}

func (o *orderer) Swap(i, j int) {
	o.Entities[i], o.Entities[j] = o.Entities[j], o.Entities[i]
}

func findNextProp(e *ds.EntityProto, p string, last int) int {
	for i := last + 1; i < len(e.Property); i++ {
		if e.Property[i].GetName() == p {
			return i
		}
	}
	return -1
}

func (o *orderer) compare(lhs, rhs *ds.EntityProto) bool {
	for _, d := range o.Order {
		reverse := d.GetDirection() == ds.Query_Order_DESCENDING
		lhi := findNextProp(lhs, d.GetProperty(), -1)
		rhi := findNextProp(rhs, d.GetProperty(), -1)
		for lhi >= 0 && rhi >= 0 {
			lhv, rhv := lhs.Property[lhi].Value, rhs.Property[rhi].Value
			if compareValue(ds.Query_Filter_LESS_THAN, lhv, rhv) {
				return !reverse
			} else if compareValue(ds.Query_Filter_LESS_THAN, rhv, lhv) {
				return reverse
			}
			lhi = findNextProp(lhs, d.GetProperty(), lhi)
			rhi = findNextProp(rhs, d.GetProperty(), rhi)
		}
		if rhi < 0 && lhi >= 0 {
			return reverse // The right hand side of this prop shorter, so it is less.
		}
	}

	return false
}
