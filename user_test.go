package fae

import (
	"testing"

	"appengine/user"
)

func TestNoUser(t *testing.T) {
	ctx := New()

	u := user.Current(ctx)
	if u != nil {
		t.Error("didn't expect user, got ", u)
	}
}

func TestLogin(t *testing.T) {
	ctx := New()

	u := &user.User{
		Email: "mike@example.com",
		Admin: false,
		ID:    "mike@example.com",
	}
	ctx.Login(u)

	u2 := user.Current(ctx)
	if u2 == nil {
		t.Fatal("user not found")
	}

	if u.Email != u2.Email {
		t.Error("user doesn't match: ", u2)
	}
}

func TestLoginAsAdmin(t *testing.T) {
	ctx := New()

	u := &user.User{
		Email: "mike@example.com",
		Admin: true,
		ID:    "mike@example.com",
	}
	ctx.Login(u)

	u2 := user.Current(ctx)
	if u2 == nil {
		t.Fatal("user not found")
	}

	if u.Email != u2.Email {
		t.Error("user doesn't match: ", u2)
	}
	if !u2.Admin {
		t.Error("user isn't admin: ", u2)
	}
}

func TestLogout(t *testing.T) {
	ctx := New()

	u := &user.User{
		Email: "mike@example.com",
		Admin: false,
		ID:    "mike@example.com",
	}
	ctx.Login(u)
	ctx.Logout()

	u2 := user.Current(ctx)
	if u2 != nil {
		t.Error("didn't expect user, got ", u)
	}
}
