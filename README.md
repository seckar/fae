Fae -- Fake App Engine
----------------------

A lightweight fake of appengine for use in unit tests.

Constructing a test environment is much faster with this library than when using
App Engine's aetest package, which makes the edit/compile/test cycle much
shorter. However the implementation is partial (e.g. no memcache or blobstore
for now) and unlikely to match the semantics of the original API in all cases.

### TODOs

* add flag to allow running against dev_appserver instead of the fake, so tests can be run in fast or realistic mode
* implement appengine/users
* add Environment, which can create multiple contexts which share the same datastore
* verify semantics of queries against repeated properties match AE

### License

The MIT License (MIT)

Copyright (c) 2014 Nicholas Seckar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
