// Package fae provides a fake App Engine environment for unit tests.
package fae

import (
	"fmt"
	"hash/crc32"
	"net/http"
	"reflect"
	"runtime"
	"strconv"

	"bitbucket.org/seckar/fae/datastore"

	"code.google.com/p/goprotobuf/proto"

	aeuser "appengine/user"
	"appengine_internal"
	aebase "appengine_internal/base"
)

const (
	hEmail             = "X-AppEngine-User-Email"
	hFederatedIdentity = "X-AppEngine-User-Federated-Identity"
	hFederatedProvider = "X-AppEngine-User-Federated-Provider"
	hID                = "X-AppEngine-User-Id"
	hIsAdmin           = "X-AppEngine-User-Is-Admin"
)

type Context struct {
	datastore *datastore.Instance
	request   http.Request
}

func log(level, format string, args []interface{}) {
	var c string
	if _, file, line, ok := runtime.Caller(2); ok {
		c = fmt.Sprintf("%16s:%d:", file, line)
	} else {
		c = "<unknown file>"
	}
	fmt.Printf("%s %s: %s\n", c, level, fmt.Sprintf(format, args...))
}

//// User support

func (c *Context) Login(u *aeuser.User) {
	if c.request.Header == nil {
		c.request.Header = make(map[string][]string)
	}
	c.request.Header.Set(hEmail, u.Email)
	c.request.Header.Set(hFederatedIdentity, u.FederatedIdentity)
	c.request.Header.Set(hFederatedProvider, u.FederatedProvider)

	if u.Admin {
		c.request.Header.Set(hIsAdmin, "1")
	} else {
		c.request.Header.Set(hIsAdmin, "0")
	}

	id := u.ID
	if id == "" {
		id = strconv.Itoa(int(crc32.Checksum([]byte(u.Email), crc32.IEEETable)))
	}
	c.request.Header.Set(hID, id)
}

func (c *Context) Logout() {
	if c.request.Header != nil {
		c.request.Header.Del(hEmail)
		c.request.Header.Del(hFederatedIdentity)
		c.request.Header.Del(hFederatedProvider)
		c.request.Header.Del(hID)
		c.request.Header.Del(hIsAdmin)
	}
}

func (c *Context) handleGoRequest(
	method string,
	in, out appengine_internal.ProtoMessage,
	opts *appengine_internal.CallOptions) error {
	out.(*aebase.StringProto).Value = proto.String("")
	return nil
}

//// appengine context interface

func (c *Context) Debugf(format string, args ...interface{}) {
	log("DEBUG", format, args)
}
func (c *Context) Infof(format string, args ...interface{}) {
	log("INFO", format, args)
}
func (c *Context) Warningf(format string, args ...interface{}) {
	log("WARNING", format, args)
}
func (c *Context) Errorf(format string, args ...interface{}) {
	log("ERROR", format, args)
}
func (c *Context) Criticalf(format string, args ...interface{}) {
	log("CRITICAL", format, args)
}

func (c *Context) Call(
	service, method string,
	in, out appengine_internal.ProtoMessage,
	opts *appengine_internal.CallOptions) error {
	switch service {
	case "datastore_v3":
		return c.datastore.HandleRequest(service, method, in, out, opts)
	case "__go__":
		return c.handleGoRequest(method, in, out, opts)
	}

	c.Warningf("Fae does not support %s.%s (%s -> %s)",
		service, method, reflect.TypeOf(in), reflect.TypeOf(out))

	return fmt.Errorf(
		"%s.%s (%s -> %s) not implemented",
		service, method, reflect.TypeOf(in), reflect.TypeOf(out))
}

func (c *Context) FullyQualifiedAppID() string {
	return "testApp"
}

func (c *Context) Request() interface{} {
	return &c.request
}

//// context construction

func New() *Context {
	return &Context{datastore: datastore.NewInstance()}
}
