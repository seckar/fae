package fae

import (
	"testing"

	aeds "appengine/datastore"
)

func check(t *testing.T, err error) {
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
}

type Address struct {
	Line    string
	Country string
}

type Person struct {
	First, Last string
	Address     Address
}

func TestPutCompleteKey(t *testing.T) {
	c := New()
	p := Person{First: "John", Last: "Doe"}
	k := aeds.NewKey(c, "Person", "johndoe", 0, nil)
	_, err := aeds.Put(c, k, &p)
	check(t, err)
}

func TestPutCompleteKeyWithoutField(t *testing.T) {
	t.Skip("impossible to tell this case from TestPutIncompleteKey")

	c := New()
	p := Person{First: "John", Last: "Doe"}
	k := aeds.NewKey(c, "Person", "", 0, nil)
	_, err := aeds.Put(c, k, &p)
	if err != aeds.ErrInvalidKey {
		t.Error("expected ErrInvalidKey, got", err)
	}
}

func TestPutIncompleteKey(t *testing.T) {
	c := New()
	p := Person{First: "John", Last: "Doe"}
	k := aeds.NewIncompleteKey(c, "Person", nil)
	k, err := aeds.Put(c, k, &p)
	check(t, err)

	var p2 Person
	check(t, aeds.Get(c, k, &p2))
}

func TestGetNonExistent(t *testing.T) {
	c := New()
	var p Person
	err := aeds.Get(c, aeds.NewKey(c, "Person", "mike", 0, nil), &p)
	if err != aeds.ErrNoSuchEntity {
		t.Error("expected ErrNoSuchEntity, got", err)
	}
}

func TestDelete(t *testing.T) {
	c := New()
	p := Person{First: "John", Last: "Doe"}
	k := aeds.NewKey(c, "Person", "johndoe", 0, nil)
	_, err := aeds.Put(c, k, &p)
	check(t, err)

	check(t, aeds.Delete(c, k))

	// Make sure the entity is deleted
	err = aeds.Get(c, k, &p)
	if err != aeds.ErrNoSuchEntity {
		t.Error("expected ErrNoSuchEntity, got", err)
	}
}

func TestQueryEmptyInstance(t *testing.T) {
	c := New()
	var ps []Person
	_, err := aeds.NewQuery("Person").GetAll(c, &ps)
	check(t, err)
	if len(ps) != 0 {
		t.Error("should be empty, got", ps)
	}
}

func TestQueryOneInstance(t *testing.T) {
	c := New()
	p := Person{First: "John", Last: "Doe"}
	k := aeds.NewKey(c, "Person", "johndoe", 0, nil)
	_, err := aeds.Put(c, k, &p)
	check(t, err)

	var ps []Person
	_, err = aeds.NewQuery("Person").GetAll(c, &ps)
	check(t, err)
	if len(ps) != 1 {
		t.Error("should have one result, got", ps)
	}
}

func TestQueryFilterField(t *testing.T) {
	c := New()
	p := Person{
		First: "John",
		Last:  "Doe",
		Address: Address{
			Country: "USA",
		},
	}

	k := aeds.NewKey(c, "Person", "johndoe", 0, nil)
	_, err := aeds.Put(c, k, &p)
	check(t, err)

	var ps []Person
	_, err = aeds.NewQuery("Person").Filter("First =", "Mike").GetAll(c, &ps)
	check(t, err)
	if len(ps) != 0 {
		t.Error("should have zero results, got", ps)
	}

	ps = ps[:0]
	_, err = aeds.NewQuery("Person").Filter("First =", "John").GetAll(c, &ps)
	check(t, err)
	if len(ps) != 1 {
		t.Error("should have one result, got", ps)
	}
}

func TestQueryFilterFieldTwice(t *testing.T) {
	c := New()
	p := Person{
		First: "John",
		Last:  "Doe",
		Address: Address{
			Country: "USA",
		},
	}

	k := aeds.NewKey(c, "Person", "johndoe", 0, nil)
	_, err := aeds.Put(c, k, &p)
	check(t, err)

	var ps []Person
	_, err = aeds.NewQuery("Person").
		Filter("First =", "John").
		Filter("First =", "Mike").
		GetAll(c, &ps)
	check(t, err)
	if len(ps) != 0 {
		t.Error("should have no results, got", ps)
	}
}

func TestQueryWithOrder(t *testing.T) {
	c := New()
	john := Person{First: "John", Last: "Doe"}
	susan := Person{First: "Susan", Last: "Black"}

	_, err := aeds.Put(c, aeds.NewKey(c, "Person", "johndoe", 0, nil), &john)
	check(t, err)
	_, err = aeds.Put(c, aeds.NewKey(c, "Person", "susanblack", 0, nil), &susan)
	check(t, err)

	var ps []Person
	_, err = aeds.NewQuery("Person").Order("First").GetAll(c, &ps)
	check(t, err)
	if len(ps) != 2 {
		t.Error("should have 2 results, got", ps)
		return
	}
	if ps[0].First != "John" {
		t.Error("wrong order", ps)
		return
	}

	ps = ps[:0]
	_, err = aeds.NewQuery("Person").Order("-First").GetAll(c, &ps)
	check(t, err)
	if len(ps) != 2 {
		t.Error("should have 2 results, got", ps)
		return
	}
	if ps[0].First != "Susan" {
		t.Error("wrong order", ps)
		return
	}
}

func TestQueryWithMultipleOrders(t *testing.T) {
	c := New()
	doe := Person{First: "John", Last: "Doe"}
	black := Person{First: "John", Last: "Black"}

	_, err := aeds.Put(c, aeds.NewKey(c, "Person", "doe", 0, nil), &doe)
	check(t, err)
	_, err = aeds.Put(c, aeds.NewKey(c, "Person", "black", 0, nil), &black)
	check(t, err)

	var ps []Person
	_, err = aeds.NewQuery("Person").
		Order("First").
		Order("Last").
		GetAll(c, &ps)
	check(t, err)
	if len(ps) != 2 {
		t.Error("should have 2 results, got", ps)
		return
	}
	if ps[0].Last != "Black" {
		t.Error("wrong order", ps)
		return
	}

	ps = ps[:0]
	_, err = aeds.NewQuery("Person").
		Order("First").
		Order("-Last").
		GetAll(c, &ps)
	check(t, err)
	if len(ps) != 2 {
		t.Error("should have 2 results, got", ps)
		return
	}
	if ps[0].Last != "Doe" {
		t.Error("wrong order", ps)
		return
	}
}
